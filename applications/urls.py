from django.urls import path
from applications import views

urlpatterns = [
    path('applications/', views.application_list),
    path('applications/<int:pk>/', views.application_detail),
]