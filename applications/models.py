# Create your models here.

from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted([(item, item) for item in get_all_styles()])


class Application(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, blank=False, default='')
    description = models.TextField()
    os = models.CharField(max_length=16, blank=True, default='')
    type = models.CharField(max_length=16, blank=True, default='')
    store_name = models.CharField(max_length=32, blank=True, default='')
    identifier = models.CharField(max_length=64, blank=True, default='')
    managed = models.CharField(max_length=32, blank=True, default='')
    attributes = models.CharField(max_length=32, blank=True, default='')
    management_flags = models.CharField(max_length=32, blank=True, default='')
    templatesName = models.CharField(max_length=32, blank=True, default='')

