from rest_framework import serializers
from .models import Application, LANGUAGE_CHOICES, STYLE_CHOICES


class ApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = ['id', 'name', 'description', 'os', 'type', 'store_name', 'identifier']