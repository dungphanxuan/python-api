from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from .models import Application
from .serializers import ApplicationSerializer


@csrf_exempt
def application_list(request):
    """
    List all code applications, or create a new application.
    """
    if request.method == 'GET':
        applications = Application.objects.all()
        serializer = ApplicationSerializer(applications, many=True)
        # Todo Limit ban ghi, phan trang
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        # Todo validate
        data = JSONParser().parse(request)
        serializer = ApplicationSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def application_detail(request, pk):
    """
    Retrieve, update or delete a code application.
    """
    try:
        application = Application.objects.get(pk=pk)
    except Application.DoesNotExist:
        messs = {
            "msg": "Application not found!",
            "code": 404
        }
        return JsonResponse(messs)

    if request.method == 'GET':
        serializer = ApplicationSerializer(application)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ApplicationSerializer(application, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        application.delete()
        return HttpResponse(status=204)
